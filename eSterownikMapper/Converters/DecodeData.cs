﻿using eSterownikMapper.Model;
using Newtonsoft.Json;

namespace eSterownikMapper.Converters;

public static class DecodeData
{
    public static string DecodeDataFromSyncValuesAsJson(string dataFromECoal)
    {
        try
        {
            // Odczytujemy segmenty danych
            string[] dataSegments = dataFromECoal.Split("\r");

            // Kasujemy puste segmenty
            dataSegments = dataSegments.Where(x => x.Length > 1).ToArray();

            // Odczytujemy pierwszy segment
            string firstDataSegment = dataSegments[0];

            // Tworzymy słownik danych
            Dictionary<string, string> segmentDictionary = new();

            // Odczytujemy dane z 1 segmentu
            string[] readedSegment = firstDataSegment.Split(';');
            // Kasujemy puste
            readedSegment = readedSegment.Where(x => x.Length > 1).ToArray();

            // Pierwszy oznacza czas odczytu??
            segmentDictionary.Add("readed_date", readedSegment[0]);

            // Dla każdego wierwsza z pierwszego segmentu
            for (int index = 1; index < readedSegment.Length; index++)
            {
                // Odczytujemy wiersz
                string segment = readedSegment[index];
                // Dzielimy dane
                string[] readedData = segment.Split(':');

                // Zamieniamy znaki HTML na normalne
                string value = readedData[1].Replace("%2", ",").Replace("%3D", "=");

                // Dodajemy do słownika
                segmentDictionary.Add(readedData[0], value);
            }

            return JsonConvert.SerializeObject(segmentDictionary);
        }
        catch (Exception exception)
        {
            Console.WriteLine("Wystapił błąd podczas dekodowania danych:");
            Console.WriteLine($"{exception.GetType().Name} - {typeof(DecodeData)} - {exception.Message}");
        }

        return "";
    }
    public static ECoalSync? DecodeDataFromSyncValues(string dataFromECoal)
    {
        ECoalSync? eCoalSync = new();
        try
        {
            // Serialzujemy na Jsona
            string json = DecodeDataFromSyncValuesAsJson(dataFromECoal);

            // Deserializujemy
            eCoalSync = JsonConvert.DeserializeObject<ECoalSync>(json);
        }
        catch (Exception exception)
        {
            Console.WriteLine("Wystapił błąd podczas dekodowania danych:");
            Console.WriteLine($"{exception.GetType().Name} - {typeof(DecodeData)} - {exception.Message}");
        }

        return eCoalSync;
    }
}