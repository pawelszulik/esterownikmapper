﻿using Newtonsoft.Json;

namespace eSterownikMapper.Converters;

public class JsonConverters
{
    public class FromUnixDateTimeSecondsConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object? existingValue, JsonSerializer serializer)
        {
            if (!int.TryParse((string?)reader.Value, out int time))
                return DateTime.MinValue;
            return DateTimeOffset.FromUnixTimeSeconds(time).UtcDateTime;
        }

        public override void WriteJson(JsonWriter writer, object? value, JsonSerializer serializer)
        {
            this.WriteJson(writer, value, serializer);
        }
    }
}