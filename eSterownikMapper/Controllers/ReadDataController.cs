using eSterownikMapper.Converters;
using eSterownikMapper.Model;
using eSterownikMapper.WebApi;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Xml;

namespace eSterownikMapper.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReadDataController : ControllerBase
    {
        [HttpGet("GetAllValues")]
        public ActionResult<ECoalSync> GetAllValues()
        {
            ECoalSync? eCoalSync = default;
            string? readed_data = string.Empty;

            if (StaticValues.LastDownload.GetValueOrDefault(DateTime.Now.AddHours(-1)).AddSeconds(10) >= DateTime.Now
                && StaticValues.LastECoalSync != null)
            {
                return StaticValues.LastECoalSync;
            }
            try
            {
                readed_data = WebApiHelper.GetDataFromWeb<string>(StaticValues.URL, StaticValues.Login, StaticValues.Password);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            if (readed_data == null)
                return NotFound("Problem podczas pobierania, zwr�cono pust� warto��");
            
            eCoalSync = StaticValues.LastECoalSync = DecodeData.DecodeDataFromSyncValues(readed_data);
            StaticValues.LastDownload = DateTime.Now;

            return Ok(eCoalSync);
        }

        [HttpGet("GetField")]
        public IActionResult GetField(string fieldName)
        {
            var property = typeof(ECoalSync).GetProperty(fieldName);
            if (property == null)
            {
                return NotFound();
            }

            ECoalSync? eCoalSync = default;
            string? readed_data = string.Empty;

            if (StaticValues.LastDownload.GetValueOrDefault(DateTime.Now.AddHours(-1)).AddSeconds(10) >= DateTime.Now
                && StaticValues.LastECoalSync != null)
            {
                return Ok(property.GetValue(StaticValues.LastECoalSync));
            }

            try
            {
                readed_data = WebApiHelper.GetDataFromWeb<string>(StaticValues.URL, StaticValues.Login, StaticValues.Password);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }

            if (readed_data == null)
                return NotFound("Problem podczas pobierania, zwr�cono pust� warto��");

            eCoalSync = StaticValues.LastECoalSync = DecodeData.DecodeDataFromSyncValues(readed_data);
            StaticValues.LastDownload = DateTime.Now;

            var value = property.GetValue(eCoalSync);

            return Ok(value);
        }

        [HttpGet("SetField")]
        public IActionResult SetField(string fieldName, string value)
        {
            var property = typeof(ECoalSync).GetProperty(fieldName);
            var jsonPropertyAttribute = property.GetCustomAttribute<JsonPropertyAttribute>();
            string? jsonPropertyName;

            if (jsonPropertyAttribute != null)
            {
                jsonPropertyName = jsonPropertyAttribute.PropertyName;
            }
            else
            {
                return NotFound();
            }

            string apiResult = String.Empty;

            try
            {
                apiResult = WebApiHelper.SetData<string>(StaticValues.IpAddress, StaticValues.Login, StaticValues.Password, jsonPropertyName, value) ?? throw new Exception("Api zwr�ci�o pusto");
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status400BadRequest, e.Message);
            }

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(apiResult);

            XmlNode cmdNode = xmlDoc.SelectSingleNode("/cmd");
            if (cmdNode != null)
            {
                string status = cmdNode.Attributes["status"]?.Value;
                if (status != null && status.Equals("ok", StringComparison.OrdinalIgnoreCase))
                {
                    return Ok(status);
                }
                else
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, status);
                }
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Nie mo�na znale�� elementu cmd.");
            }
        }
    }
}