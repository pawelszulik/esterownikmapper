﻿using System.ComponentModel;
using eSterownikMapper.Converters;
using Newtonsoft.Json;

namespace eSterownikMapper.Model;

public class ECoalSync
{

    /// <summary>
    /// Data odczytu
    /// </summary>
    [JsonProperty("readed_date")]
    [JsonConverter(typeof(JsonConverters.FromUnixDateTimeSecondsConverter))]
    [DisplayName("Data odczytu")]
    public DateTime ReadDate { get; set; }

    /// <summary>
    /// Zegar czasu rzeczywistego
    /// </summary>
    [JsonProperty("rtc_calib_en")]
    [DisplayName("Zegar czasu rzeczywistego")]
    public string? RealTimeClockCalibrationEnglish { get; set; }

    /// <summary>
    /// Korekcja zegara czasu rzeczywistego
    /// </summary>
    [JsonProperty("rtc_correction")]
    [DisplayName("Korekcja zegara czasu rzeczywistego")]
    public string? RealTimeClockCorrection { get; set; }

    /// <summary>
    /// Numer Seryjny
    /// </summary>
    [JsonProperty("device_sn")]
    [DisplayName("Numer Seryjny")]
    public string? DeviceSerialNumber { get; set; }

    /// <summary>
    /// Identyfikator urządzenia
    /// </summary>
    [JsonProperty("device_id")]
    [DisplayName("Identyfikator urządzenia")]
    public string? DeviceId { get; set; }

    /// <summary>
    /// Nazwa urządzenia
    /// </summary>
    [JsonProperty("device_name")]
    [DisplayName("Nazwa urządzenia")]
    public string? DeviceName { get; set; }

    /// <summary>
    /// Lokalizacja urządzenia
    /// </summary>
    [JsonProperty("device_location")]
    [DisplayName("Lokalizacja urządzenia")]
    public string? DeviceLocation { get; set; }

    /// <summary>
    /// Typ urządzenia
    /// </summary>
    [JsonProperty("device_type")]
    [DisplayName("Typ urządzenia")]
    public string? DeviceType { get; set; }

    /// <summary>
    /// Wersja oprogramowania
    /// </summary>
    [JsonProperty("device_soft_version")]
    [DisplayName("Wersja oprogramowania")]
    public string? DeviceSoftwareVersion { get; set; }

    /// <summary>
    /// Wersja sprzętu
    /// </summary>
    [JsonProperty("device_hard_version")]
    [DisplayName("Wersja sprzętu")]
    public string? HardwareVersion { get; set; }

    /// <summary>
    /// Autoryzacja użytkownika root
    /// </summary>
    [JsonProperty("auth_root")]
    [DisplayName("Autoryzacja użytkownika root")]
    public string? AuthorizationRoot { get; set; }

    /// <summary>
    /// Autoryzacja użytkownika Admin
    /// </summary>
    [JsonProperty("auth_admin")]
    [DisplayName("Autoryzacja użytkownika Admin")]
    public string? AuthorizationAdmin { get; set; }

    /// <summary>
    /// Autoryzacja użytkownika User
    /// </summary>
    [JsonProperty("auth_user")]
    [DisplayName("Autoryzacja użytkownika User")]
    public string? AuthorizationUser { get; set; }

    /// <summary>
    /// Poziom dostępu
    /// </summary>
    [JsonProperty("accesslevel")]
    [DisplayName("Poziom dostępu")]
    public int AccessLevel { get; set; }

    /// <summary>
    /// Aktualny czas sterownika
    /// </summary>
    [JsonProperty("datetime")]
    [DisplayName("Aktualny czas sterownika")]
    [Newtonsoft.Json.JsonConverter(typeof(JsonConverters.FromUnixDateTimeSecondsConverter))]
    public DateTime? DateTime { get; set; }

    /// <summary>
    /// Data
    /// </summary>
    [JsonProperty("date")]
    [DisplayName("Data")]
    public long Date { get; set; }

    /// <summary>
    /// Czas
    /// </summary>
    [JsonProperty("time")]
    [DisplayName("Czas")]
    public long Time { get; set; }

    /// <summary>
    /// Lokalna strefa czasowa
    /// </summary>
    [JsonProperty("localtimezone")]
    [DisplayName("Lokalna strefa czasowa")]
    public string? LocalTimezone { get; set; }

    /// <summary>
    /// Adres MAC
    /// </summary>
    [JsonProperty("eth_mac")]
    [DisplayName("Adres MAC")]
    public string? EthernetMAC { get; set; }

    /// <summary>
    /// Skofigurowany adres IP
    /// </summary>
    [JsonProperty("eth_ip")]
    [DisplayName("Skofigurowany adres IP")]
    public string? EthernetIPAddressConfigured { get; set; }

    /// <summary>
    /// Maska sieci
    /// </summary>
    [JsonProperty("eth_mask")]
    [DisplayName("Maska sieci")]
    public string? EthernetMaskConfigured { get; set; }

    /// <summary>
    /// Brama
    /// </summary>
    [JsonProperty("eth_gate")]
    [DisplayName("Brama")]
    public string? EthernetGate { get; set; }

    /// <summary>
    /// DHCP
    /// </summary>
    [JsonProperty("eth_dhcp")]
    [DisplayName("DHCP")]
    public string? EthernetDHCP { get; set; }

    /// <summary>
    /// Adres IP
    /// </summary>
    [JsonProperty("eth_ip_ro")]
    [DisplayName("Adres IP")]
    public string? EthernetIPAddressCurrent { get; set; }

    /// <summary>
    /// Maska sieci
    /// </summary>
    [JsonProperty("eth_mask_ro")]
    [DisplayName("Maska sieci")]
    public string? EthernetMaskCurrent { get; set; }

    /// <summary>
    /// Brama
    /// </summary>
    [JsonProperty("eth_gate_ro")]
    [DisplayName("Brama")]
    public string? EthernetGatewayCurrent { get; set; }

    /// <summary>
    /// Interfejs ethernetowy
    /// </summary>
    [JsonProperty("eth_iface")]
    [DisplayName("Interfejs ethernetowy")]
    public int EthernetInterface { get; set; }

    /// <summary>
    /// Zdalne połączenie
    /// </summary>
    [JsonProperty("remote_server_status")]
    [DisplayName("Zdalne połączenie")]
    public int RemoteServerStatus { get; set; }

    /// <summary>
    /// Język
    /// </summary>
    [JsonProperty("lang")]
    [DisplayName("Język")]
    public int Language { get; set; }

    [JsonProperty("pattern_lang")]
    [DisplayName("")]
    public string? PatternLang { get; set; }

    /// <summary>
    /// Wartość skuteczna dmuchawy/nawiewnika
    /// </summary>
    [JsonProperty("dm_rms")]
    [DisplayName("Wartość skuteczna dmuchawy/nawiewnika")]
    public string? DiffuserMotorRootMeanSquare { get; set; }

    [JsonProperty("mod1")]
    [DisplayName("")]
    public string? Mod1 { get; set; }

    [JsonProperty("mod2")]
    [DisplayName("")]
    public string? Mod2 { get; set; }

    [JsonProperty("mod1_tacho_speed")]
    [DisplayName("")]
    public string? Mod1TachoSpeed { get; set; }

    [JsonProperty("mod1_tacho_max_speed")]
    [DisplayName("")]
    public string? Mod1TachoMaxSpeed { get; set; }

    /// <summary>
    /// Moduł RF
    /// </summary>
    [JsonProperty("rf_module")]
    [DisplayName("Moduł RF")]
    public string? RfModule { get; set; }

    [JsonProperty("rf_update")]
    [DisplayName("")]
    public string? RfUpdate { get; set; }

    [JsonProperty("rf_offset")]
    [DisplayName("")]
    public string? RfOffset { get; set; }

    [JsonProperty("log")]
    [DisplayName("")]
    public string? Log { get; set; }

    [JsonProperty("screen_code")]
    [DisplayName("")]
    public string? ScreenCode { get; set; }

    /// <summary>
    /// Stan zasilania
    /// </summary>
    [JsonProperty("power_state")]
    [DisplayName("Stan zasilania")]
    public string? PowerState { get; set; }

    /// <summary>
    /// Temperatura kotła
    /// </summary>
    [JsonProperty("tkot_value")]
    [DisplayName("Temperatura kotła")]
    public decimal? KettleTemperature { get; set; }

    /// <summary>
    /// Korekta kotła
    /// </summary>
    [JsonProperty("tkot_cal")]
    [DisplayName("Korekta kotła")]
    public decimal? KettleCorrection { get; set; }

    /// <summary>
    /// Temperatura powrotu
    /// </summary>
    [JsonProperty("tpow_value")]
    [DisplayName("Temperatura powrotu")]
    public decimal? ReturnTemperature { get; set; }

    /// <summary>
    /// Korekta powrotu
    /// </summary>
    [JsonProperty("tpow_cal")]
    [DisplayName("Korekta powrotu")]
    public decimal? ReturnCorrection { get; set; }

    /// <summary>
    /// Temperatura podajnika
    /// </summary>
    [JsonProperty("tpod_value")]
    [DisplayName("Temperatura podajnika")]
    public decimal? FeederTemperature { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik podajnika
    /// </summary>
    [JsonProperty("tpod_cal")]
    [DisplayName("Poprawka kalibracyjna - czujnik podajnika")]
    public decimal? FeederTemperatureCorrection { get; set; }

    /// <summary>
    /// Temperatura CWU
    /// </summary>
    [JsonProperty("tcwu_value")]
    [DisplayName("Temperatura CWU")]
    public decimal? HUWTemperature { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik CWU
    /// </summary>
    [JsonProperty("tcwu_cal")]
    [DisplayName("Poprawka kalibracyjna - czujnik CWU")]
    public decimal? HUWTemperatureCorrection { get; set; }

    /// <summary>
    /// Temperatura wewnętrzna
    /// </summary>
    [JsonProperty("twew_value")]
    [DisplayName("Temperatura wewnętrzna")]
    public decimal? InternalTemperature { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik wewnętrzny CO1
    /// </summary>
    [JsonProperty("twew_cal")]
    [DisplayName("Poprawka kalibracyjna - czujnik wewnętrzny CO1")]
    public decimal? InternalTemperatureCorrection { get; set; }

    /// <summary>
    /// Temperatura zewnętrzna
    /// </summary>
    [JsonProperty("tzew_value")]
    [DisplayName("Temperatura zewnętrzna")]
    public decimal? OutdoorTemperature { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik zewnętrzny
    /// </summary>
    [JsonProperty("tzew_cal")]
    [DisplayName("Poprawka kalibracyjna - czujnik zewnętrzny")]
    public decimal? OutdoorTemperatureCorrection { get; set; }

    /// <summary>
    /// Temperatura za zaworem
    /// </summary>
    [JsonProperty("t1_value")]
    [DisplayName("Temperatura za zaworem")]
    public decimal? T1Value { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik za zaworem
    /// </summary>
    [JsonProperty("t1_cal")]
    [DisplayName("Poprawka kalibracyjna - czujnik za zaworem")]
    public decimal? T1Cal { get; set; }

    /// <summary>
    /// Temperatura wewnętrzna CO2
    /// </summary>
    [JsonProperty("t2_value")]
    [DisplayName("Temperatura wewnętrzna CO2")]
    public decimal? T2Value { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik wewnętrzny CO2
    /// </summary>
    [JsonProperty("t2_cal")]
    [DisplayName("Poprawka kalibracyjna - czujnik wewnętrzny CO2")]
    public decimal? T2Cal { get; set; }

    /// <summary>
    /// Temperatura spalin
    /// </summary>
    [JsonProperty("tsp_value")]
    [DisplayName("Temperatura spalin")]
    public decimal? ExhaustTemperature { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik spalin
    /// </summary>
    [JsonProperty("tsp_cal")]
    [DisplayName("Poprawka kalibracyjna - czujnik spalin")]
    public decimal? TspCal { get; set; }

    /// <summary>
    /// Temperatura zewnętrzna
    /// </summary>
    [JsonProperty("tzew_act")]
    [DisplayName("Temperatura zewnętrzna")]
    public string? TzewAct { get; set; }

    [JsonProperty("di_zawl")]
    [DisplayName("")]
    public string? DiZawl { get; set; }

    [JsonProperty("di_zas")]
    [DisplayName("")]
    public string? DiZas { get; set; }

    [JsonProperty("di_alarm")]
    [DisplayName("")]
    public string? DiAlarm { get; set; }

    [JsonProperty("di_termik")]
    [DisplayName("")]
    public string? DiTermik { get; set; }

    [JsonProperty("di_stb")]
    [DisplayName("")]
    public string? DiStb { get; set; }

    [JsonProperty("di_rs_reset")]
    [DisplayName("")]
    public string? DiRsReset { get; set; }

    [JsonProperty("di_rs_out")]
    [DisplayName("")]
    public string? DiRsOut { get; set; }

    [JsonProperty("di_pod_state")]
    [DisplayName("")]
    public string? DiPodState { get; set; }

    [JsonProperty("di_term1")]
    [DisplayName("")]
    public string? DiTerm1 { get; set; }

    [JsonProperty("di_term2")]
    [DisplayName("")]
    public string? DiTerm2 { get; set; }

    /// <summary>
    /// Pompa 1
    /// </summary>
    [JsonProperty("out_pomp1")]
    [DisplayName("Pompa 1")]
    public string? OutPomp1 { get; set; }

    [JsonProperty("out_pomp2")]
    [DisplayName("")]
    public string? OutPomp2 { get; set; }

    /// <summary>
    /// Pompa CWU
    /// </summary>
    [JsonProperty("out_cwu")]
    [DisplayName("Pompa CWU")]
    public string? OutCwu { get; set; }

    /// <summary>
    /// Pompa dodatkowa
    /// </summary>
    [JsonProperty("out_miesz")]
    [DisplayName("Pompa dodatkowa")]
    public string? OutMiesz { get; set; }

    /// <summary>
    /// Podajnik
    /// </summary>
    [JsonProperty("out_pod")]
    [DisplayName("Podajnik")]
    public string? OutPod { get; set; }

    /// <summary>
    /// Dmuchawa
    /// </summary>
    [JsonProperty("out_dm")]
    [DisplayName("Dmuchawa")]
    public string? OutDiffuserMotor { get; set; }

    [JsonProperty("out_zaw4d")]
    [DisplayName("")]
    public string? OutZaw4d { get; set; }

    [JsonProperty("out_aux")]
    [DisplayName("")]
    public string? OutAux { get; set; }

    [JsonProperty("alarm_ipconflict")]
    [DisplayName("")]
    public string? AlarmIpconflict { get; set; }

    [JsonProperty("alarm_sdcard")]
    [DisplayName("")]
    public string? AlarmSdcard { get; set; }

    [JsonProperty("alarm_tkot")]
    [DisplayName("")]
    public string? AlarmTkot { get; set; }

    [JsonProperty("alarm_tpow")]
    [DisplayName("")]
    public string? AlarmTpow { get; set; }

    [JsonProperty("alarm_tpod")]
    [DisplayName("")]
    public string? AlarmTpod { get; set; }

    [JsonProperty("alarm_tcwu")]
    [DisplayName("")]
    public string? AlarmTcwu { get; set; }

    [JsonProperty("alarm_twew")]
    [DisplayName("")]
    public string? AlarmTwew { get; set; }

    [JsonProperty("alarm_tzew")]
    [DisplayName("")]
    public string? AlarmTzew { get; set; }

    [JsonProperty("alarm_t1")]
    [DisplayName("")]
    public string? AlarmT1 { get; set; }

    [JsonProperty("alarm_t2")]
    [DisplayName("")]
    public string? AlarmT2 { get; set; }

    [JsonProperty("alarm_tsp")]
    [DisplayName("")]
    public string? AlarmTsp { get; set; }

    [JsonProperty("alarm_termik")]
    [DisplayName("")]
    public string? AlarmTermik { get; set; }

    [JsonProperty("alarm_tkot_90")]
    [DisplayName("")]
    public string? AlarmTkot90 { get; set; }

    [JsonProperty("alarm_wyg_grz")]
    [DisplayName("")]
    public string? AlarmWygGrz { get; set; }

    [JsonProperty("alarm_wyg_pod")]
    [DisplayName("")]
    public string? AlarmWygPod { get; set; }

    [JsonProperty("alarm_zabr")]
    [DisplayName("")]
    public string? AlarmZabr { get; set; }

    [JsonProperty("alarm_tsp_hi")]
    [DisplayName("")]
    public string? AlarmTspHi { get; set; }

    [JsonProperty("alarm_tpod_hi")]
    [DisplayName("")]
    public string? AlarmTpodHi { get; set; }

    [JsonProperty("alarm_pod_zaplon")]
    [DisplayName("")]
    public string? AlarmPodZaplon { get; set; }

    [JsonProperty("alarm_zew")]
    [DisplayName("")]
    public string? AlarmZew { get; set; }

    [JsonProperty("alarm_zasobnik")]
    [DisplayName("")]
    public string? AlarmZasobnik { get; set; }

    [JsonProperty("alarm_stb")]
    [DisplayName("")]
    public string? AlarmStb { get; set; }

    [JsonProperty("alarm_out_pod")]
    [DisplayName("")]
    public string? AlarmOutPod { get; set; }

    [JsonProperty("alarm_otw_zasob")]
    [DisplayName("")]
    public string? AlarmOtwZasob { get; set; }

    [JsonProperty("alarm_uszk_pod")]
    [DisplayName("")]
    public string? AlarmUszkPod { get; set; }

    [JsonProperty("alarm_tco1_hi")]
    [DisplayName("")]
    public string? AlarmTco1Hi { get; set; }

    /// <summary>
    /// Temperatura zadana kotła
    /// </summary>
    [JsonProperty("kot_tact")]
    [DisplayName("Temperatura zadana kotła")]
    public string? KotTact { get; set; }

    /// <summary>
    /// Programator temp. kotła
    /// </summary>
    [JsonProperty("kot_prog")]
    [DisplayName("Programator temp. kotła")]
    public string? KotProg { get; set; }

    /// <summary>
    /// Temperatura zadana kotła
    /// </summary>
    [JsonProperty("kot_tzad")]
    [DisplayName("Temperatura zadana kotła")]
    public decimal? DesiredKettledrumTemperature { get; set; }

    /// <summary>
    /// Temperatura obniżona
    /// </summary>
    [JsonProperty("kot_tobn")]
    [DisplayName("Temperatura obniżona")]
    public decimal? KotTobn { get; set; }

    /// <summary>
    /// Regulator pogodowy
    /// </summary>
    [DisplayName("Regulator pogodowy")]
    [JsonProperty("pog_en")]
    public string? PogEn { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. kotła dla -10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. kotła dla -10°C")]
    [JsonProperty("pog_krzyw1")]
    public decimal? PogKrzyw1 { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. kotła dla +10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. kotła dla +10°C")]
    [JsonProperty("pog_krzyw2")]
    public decimal? PogKrzyw2 { get; set; }

    [DisplayName("")]
    [JsonProperty("kot_status")]
    public string? KotStatus { get; set; }

    [DisplayName("")]
    [JsonProperty("kot_st_tobn")]
    public string? KotStTobn { get; set; }

    /// <summary>
    /// Aktualna moc dmuchawy
    /// </summary>
    [DisplayName("Aktualna moc dmuchawy")]
    [JsonProperty("act_dm_speed")]
    public string? ActualDiffuserMotorSpeed { get; set; }

    /// <summary>
    /// Moc dmuchawy
    /// </summary>
    [DisplayName("Moc dmuchawy")]
    [JsonProperty("r_dm_speed")]
    public decimal? RDiffuserMotorSpeed { get; set; }

    /// <summary>
    /// Histereza kotła
    /// </summary>
    [DisplayName("Histereza kotła")]
    [JsonProperty("kot_hist")]
    public decimal? KotHist { get; set; }

    /// <summary>
    /// Czas pracy podajnika
    /// </summary>
    [DisplayName("Czas pracy podajnika")]
    [JsonProperty("p_pod_on")]
    public decimal? PPodOn { get; set; }

    /// <summary>
    /// Czas postoju podajnika
    /// </summary>
    [DisplayName("Czas postoju podajnika")]
    [JsonProperty("p_pod_off")]
    public decimal? PPodOff { get; set; }

    /// <summary>
    /// Czas krótkiej przerwy
    /// </summary>
    [DisplayName("Czas krótkiej przerwy")]
    [JsonProperty("p_pod_wait")]
    public decimal? PPodWait { get; set; }

    /// <summary>
    /// Ilość powtórzeń
    /// </summary>
    [DisplayName("Ilość powtórzeń")]
    [JsonProperty("p_pod_cnt")]
    public decimal? PPodCnt { get; set; }

    /// <summary>
    /// Moc dmuchawy
    /// </summary>
    [DisplayName("Moc dmuchawy")]
    [JsonProperty("p_dm_speed")]
    public decimal? PDiffuserMotorSpeed { get; set; }

    /// <summary>
    /// Regulator temperatury spalin
    /// </summary>
    [DisplayName("Regulator temperatury spalin")]
    [JsonProperty("rr_rsp_en")]
    public string? RrRspEn { get; set; }

    /// <summary>
    /// Maksymalna temperatura spalin
    /// </summary>
    [DisplayName("Maksymalna temperatura spalin")]
    [JsonProperty("rr_rsp_tmax")]
    public decimal? RrRspTmax { get; set; }

    /// <summary>
    /// Minimalna moc dmuchawy
    /// </summary>
    [DisplayName("Minimalna moc dmuchawy")]
    [JsonProperty("rr_rsp_dm_speed")]
    public decimal? RrRspDiffuserMotorSpeed { get; set; }

    /// <summary>
    /// Czas pracy podajnika
    /// </summary>
    [DisplayName("Czas pracy podajnika")]
    [JsonProperty("rr_g_pod_on")]
    public decimal? RrGPodOn { get; set; }

    /// <summary>
    /// Czas postoju podajnika
    /// </summary>
    [DisplayName("Czas postoju podajnika")]
    [JsonProperty("rr_g_pod_off")]
    public decimal? RrGPodOff { get; set; }

    /// <summary>
    /// Moc dmuchawy
    /// </summary>
    [DisplayName("Moc dmuchawy")]
    [JsonProperty("rr_g_dm_speed")]
    public decimal? RrGDiffuserMotorSpeed { get; set; }

    [DisplayName("")]
    [JsonProperty("zas_ton")]
    public string? ZasTon { get; set; }

    [DisplayName("")]
    [JsonProperty("zas_toff")]
    public string? ZasToff { get; set; }

    [DisplayName("")]
    [JsonProperty("zas_dm_speed")]
    public string? ZasDmSpeed { get; set; }

    [DisplayName("")]
    [JsonProperty("zas_sar")]
    public string? ZasSar { get; set; }

    [DisplayName("")]
    [JsonProperty("ec_dm_corr")]
    public string? EcDmCorr { get; set; }

    [DisplayName("")]
    [JsonProperty("ec_pod_on")]
    public string? EcPodOn { get; set; }

    [DisplayName("")]
    [JsonProperty("ec_typ")]
    public string? EcTyp { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_moc_min")]
    public string? GrMocMin { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_moc_max")]
    public string? GrMocMax { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_dm_8")]
    public string? GrDm8 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_dm_12")]
    public string? GrDm12 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_dm_16")]
    public string? GrDm16 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_dm_20")]
    public string? GrDm20 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_dm_25")]
    public string? GrDm25 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_dm_30")]
    public string? GrDm30 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_cnt_8")]
    public string? GrCnt8 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_cnt_12")]
    public string? GrCnt12 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_cnt_16")]
    public string? GrCnt16 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_cnt_20")]
    public string? GrCnt20 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_cnt_25")]
    public string? GrCnt25 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_cnt_30")]
    public string? GrCnt30 { get; set; }

    [DisplayName("")]
    [JsonProperty("gr_hist")]
    public string? GrHist { get; set; }

    /// <summary>
    /// Typ obwodu
    /// </summary>
    [DisplayName("Typ obwodu")]
    [JsonProperty("ob1_typ")]
    public string? Ob1Typ { get; set; }

    /// <summary>
    /// Programator temp. wewnętrznej
    /// </summary>
    [DisplayName("Programator temp. wewnętrznej")]
    [JsonProperty("ob1_prog")]
    public string? Ob1Prog { get; set; }

    /// <summary>
    /// Temperatura zadana
    /// </summary>
    [DisplayName("Temperatura zadana")]
    [JsonProperty("ob1_tzad")]
    public decimal? Ob1Tzad { get; set; }

    /// <summary>
    /// Temperatura obniżona
    /// </summary>
    [DisplayName("Temperatura obniżona")]
    [JsonProperty("ob1_tobn")]
    public decimal? Ob1Tobn { get; set; }

    /// <summary>
    /// Temperatura maksymalna
    /// </summary>
    [DisplayName("Temperatura maksymalna")]
    [JsonProperty("ob1_tmax")]
    public decimal? Ob1Tmax { get; set; }

    /// <summary>
    /// Czas pracy pompy
    /// </summary>
    [DisplayName("Czas pracy pompy")]
    [JsonProperty("ob1_pomp_on")]
    public decimal? Ob1PompOn { get; set; }

    /// <summary>
    /// Czas postoju pompy
    /// </summary>
    [DisplayName("Czas postoju pompy")]
    [JsonProperty("ob1_pomp_off")]
    public decimal? Ob1PompOff { get; set; }

    /// <summary>
    /// Temperatura wewn. niska
    /// </summary>
    [DisplayName("Temperatura wewn. niska")]
    [JsonProperty("ob1_pok_lo")]
    public decimal? Ob1PokLo { get; set; }

    /// <summary>
    /// Temperatura wewn. normalna
    /// </summary>
    [DisplayName("Temperatura wewn. normalna")]
    [JsonProperty("ob1_pok_norm")]
    public decimal? Ob1PokNorm { get; set; }

    /// <summary>
    /// Temperatura wewn. komfortowa
    /// </summary>
    [DisplayName("Temperatura wewn. komfortowa")]
    [JsonProperty("ob1_pok_hi")]
    public decimal? Ob1PokHi { get; set; }

    /// <summary>
    /// Histereza temp. wewn.
    /// </summary>
    [DisplayName("Histereza temp. wewn.")]
    [JsonProperty("ob1_pok_hist")]
    public decimal? Ob1PokHist { get; set; }

    [DisplayName("")]
    [JsonProperty("ob1_pok_pre")]
    public string? Ob1PokPre { get; set; }

    [DisplayName("")]
    [JsonProperty("ob1_pok_tact")]
    public string? Ob1PokTact { get; set; }

    [DisplayName("")]
    [JsonProperty("ob1_pok_tzad")]
    public string? Ob1PokTzad { get; set; }

    /// <summary>
    /// Programator temp. za zaworem
    /// </summary>
    [DisplayName("Programator temp. za zaworem")]
    [JsonProperty("ob1_zaw4d_prog")]
    public string? Ob1Zaw4dProg { get; set; }

    [DisplayName("")]
    [JsonProperty("ob1_zaw4d_tzad")]
    public string? Ob1Zaw4dTzad { get; set; }

    /// <summary>
    /// Regulator pogodowy
    /// </summary>
    [DisplayName("Regulator pogodowy")]
    [JsonProperty("ob1_pog_en")]
    public string? Ob1PogEn { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. dla -10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. dla -10°C")]
    [JsonProperty("ob1_pog_krzyw1")]
    public decimal? Ob1PogKrzyw1 { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. dla +10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. dla +10°C")]
    [JsonProperty("ob1_pog_krzyw2")]
    public decimal? Ob1PogKrzyw2 { get; set; }

    /// <summary>
    /// Zatrzymanie pompy po dogrzaniu pomieszczenia
    /// </summary>
    [DisplayName("Zatrzymanie pompy po dogrzaniu pomieszczenia")]
    [JsonProperty("ob1_pump_pok")]
    public string? Ob1PumpPok { get; set; }

    [DisplayName("")]
    [JsonProperty("ob1_zaw4d_pos")]
    public string? Ob1Zaw4dPos { get; set; }

    /// <summary>
    /// Typ regulatora pokojowego
    /// </summary>
    [DisplayName("Typ regulatora pokojowego")]
    [JsonProperty("ob1_pok_typ")]
    public string? Ob1PokTyp { get; set; }

    /// <summary>
    /// Typ obwodu
    /// </summary>
    [DisplayName("Typ obwodu")]
    [JsonProperty("ob2_typ")]
    public string? Ob2Typ { get; set; }

    /// <summary>
    /// Programator temp. wewnętrznej
    /// </summary>
    [DisplayName("Programator temp. wewnętrznej")]
    [JsonProperty("ob2_prog")]
    public string? Ob2Prog { get; set; }

    [DisplayName("")]
    [JsonProperty("ob2_tzad")]
    public string? Ob2Tzad { get; set; }

    [DisplayName("")]
    [JsonProperty("ob2_tobn")]
    public string? Ob2Tobn { get; set; }

    [DisplayName("")]
    [JsonProperty("ob2_tmax")]
    public string? Ob2Tmax { get; set; }

    /// <summary>
    /// Czas pracy pompy
    /// </summary>
    [DisplayName("Czas pracy pompy")]
    [JsonProperty("ob2_pomp_on")]
    public decimal? Ob2PompOn { get; set; }

    /// <summary>
    /// Czas postoju pompy
    /// </summary>
    [DisplayName("Czas postoju pompy")]
    [JsonProperty("ob2_pomp_off")]
    public decimal? Ob2PompOff { get; set; }

    /// <summary>
    /// Temperatura wewn. niska
    /// </summary>
    [DisplayName("Temperatura wewn. niska")]
    [JsonProperty("ob2_pok_lo")]
    public decimal? Ob2PokLo { get; set; }

    /// <summary>
    /// Temperatura wewn. normalna
    /// </summary>
    [DisplayName("Temperatura wewn. normalna")]
    [JsonProperty("ob2_pok_norm")]
    public decimal? Ob2PokNorm { get; set; }

    /// <summary>
    /// Temperatura wewn. komfortowa
    /// </summary>
    [DisplayName("Temperatura wewn. komfortowa")]
    [JsonProperty("ob2_pok_hi")]
    public decimal? Ob2PokHi { get; set; }

    /// <summary>
    /// Histereza temp. wewn.
    /// </summary>
    [DisplayName("Histereza temp. wewn.")]
    [JsonProperty("ob2_pok_hist")]
    public decimal? Ob2PokHist { get; set; }

    [DisplayName("")]
    [JsonProperty("ob2_pok_pre")]
    public string? Ob2PokPre { get; set; }

    [DisplayName("")]
    [JsonProperty("ob2_pok_tact")]
    public string? Ob2PokTact { get; set; }

    [DisplayName("")]
    [JsonProperty("ob2_pok_tzad")]
    public string? Ob2PokTzad { get; set; }

    /// <summary>
    /// Typ regulatora pokojowego
    /// </summary>
    [DisplayName("Typ regulatora pokojowego")]
    [JsonProperty("ob2_pok_typ")]
    public string? Ob2PokTyp { get; set; }

    /// <summary>
    /// Tryb Zima/Lato
    /// </summary>
    [DisplayName("Tryb Zima/Lato")]
    [JsonProperty("zima_lato")]
    public string? ZimaLato { get; set; }

    /// <summary>
    /// Tryb Zima/Lato
    /// </summary>
    [DisplayName("Tryb Zima/Lato")]
    [JsonProperty("zima_lato_state")]
    public string? ZimaLatoState { get; set; }

    /// <summary>
    /// Programator Auto-Lato
    /// </summary>
    [DisplayName("Programator Auto-Lato")]
    [JsonProperty("autolato_prog")]
    public string? AutolatoProg { get; set; }

    [DisplayName("Temperatura zewnętrzna dla Auto-Lato")]
    [JsonProperty("autolato_tzew")]
    public decimal? AutolatoTzew { get; set; }

    /// <summary>
    /// Tryb Pracy
    /// </summary>
    [DisplayName("Tryb Pracy")]
    [JsonProperty("tryb_auto_state")]
    public string? TrybAutoState { get; set; }

    /// <summary>
    /// Histereza Auto-Lato
    /// </summary>
    [DisplayName("Histereza Auto-Lato")]
    [JsonProperty("autolato_hist")]
    public decimal? AutolatoHist { get; set; }

    /// <summary>
    /// Programator CWU
    /// </summary>
    [DisplayName("Programator CWU")]
    [JsonProperty("cwu_prog")]
    public string? CwuProg { get; set; }

    [DisplayName("")]
    [JsonProperty("cwu_tact")]
    public string? CwuTact { get; set; }

    /// <summary>
    /// Temperatura zadana CWU
    /// </summary>
    [DisplayName("Temperatura zadana CWU")]
    [JsonProperty("cwu_tzad")]
    public decimal? CwuTzad { get; set; }

    /// <summary>
    /// Temperatura obniżona CWU
    /// </summary>
    [DisplayName("Temperatura obniżona CWU")]
    [JsonProperty("cwu_tobn")]
    public decimal? CwuTobn { get; set; }

    /// <summary>
    /// Histereza #1
    /// </summary>
    [DisplayName("Histereza #1")]
    [JsonProperty("cwu_hist1")]
    public decimal? CwuHist1 { get; set; }

    /// <summary>
    /// Histereza #2
    /// </summary>
    [DisplayName("Histereza #2")]
    [JsonProperty("cwu_hist2")]
    public decimal? CwuHist2 { get; set; }

    /// <summary>
    /// Podbicie temp. kotła
    /// </summary>
    [DisplayName("Podbicie temp. kotła")]
    [JsonProperty("cwu_podb_kot")]
    public decimal? CwuPodbKot { get; set; }

    [DisplayName("")]
    [JsonProperty("cwu_out_state")]
    public string? CwuOutState { get; set; }

    /// <summary>
    /// Tryb CWU
    /// </summary>
    [DisplayName("Tryb CWU")]
    [JsonProperty("cwu_state")]
    public string? CwuState { get; set; }

    [DisplayName("")]
    [JsonProperty("cwu_st_tobn")]
    public string? CwuStTobn { get; set; }

    /// <summary>
    /// Programator pompy cyrkulacyjnej
    /// </summary>
    [DisplayName("Programator pompy cyrkulacyjnej")]
    [JsonProperty("cyrk_prog")]
    public string? CyrkProg { get; set; }

    /// <summary>
    /// Czas pracy
    /// </summary>
    [DisplayName("Czas pracy")]
    [JsonProperty("cyrk_pomp_on")]
    public decimal? CyrkPompOn { get; set; }

    /// <summary>
    /// Czas postoju
    /// </summary>
    [DisplayName("Czas postoju")]
    [JsonProperty("cyrk_pomp_off")]
    public decimal? CyrkPompOff { get; set; }

    /// <summary>
    /// Temp. załączenia
    /// </summary>
    [DisplayName("Temp. załączenia")]
    [JsonProperty("cyrk_pomp_ton")]
    public decimal? CyrkPompTon { get; set; }

    /// <summary>
    /// Minimalna temp. powrotu
    /// </summary>
    [DisplayName("Minimalna temp. powrotu")]
    [JsonProperty("tpow_min")]
    public decimal? TpowMin { get; set; }

    /// <summary>
    /// Temperatura załączenia pomp
    /// </summary>
    [DisplayName("Temperatura załączenia pomp")]
    [JsonProperty("pomp_ton")]
    public decimal? PompTon { get; set; }

    /// <summary>
    /// Czujnik temperatury zewnętrznej
    /// </summary>
    [DisplayName("Czujnik temperatury zewnętrznej")]
    [JsonProperty("tzew_sensor")]
    public string? TzewSensor { get; set; }

    /// <summary>
    /// Ochrona powrotu
    /// </summary>
    [DisplayName("Ochrona powrotu")]
    [JsonProperty("ochr_pow")]
    public string? OchrPow { get; set; }

    /// <summary>
    /// Funkcja pompy dodatkowej
    /// </summary>
    [DisplayName("Funkcja pompy dodatkowej")]
    [JsonProperty("pomp_ext_func")]
    public string? PompExtFunc { get; set; }

    /// <summary>
    /// Maksymalna temperatura podajnika
    /// </summary>
    [DisplayName("Maksymalna temperatura podajnika")]
    [JsonProperty("pod_tmax")]
    public decimal? PodTmax { get; set; }

    /// <summary>
    /// Czas wyrzutu paliwa
    /// </summary>
    [DisplayName("Czas wyrzutu paliwa")]
    [JsonProperty("pod_time")]
    public decimal? PodTime { get; set; }

    /// <summary>
    /// Algorytm wygaszenia kotła
    /// </summary>
    [DisplayName("Algorytm wygaszenia kotła")]
    [JsonProperty("alg_wyg_kot")]
    public string? AlgWygKot { get; set; }

    /// <summary>
    /// Typ podajnika
    /// </summary>
    [DisplayName("Typ podajnika")]
    [JsonProperty("pod_typ")]
    public string? PodTyp { get; set; }

    /// <summary>
    /// Czas opóźnienia
    /// </summary>
    [DisplayName("Czas opóźnienia")]
    [JsonProperty("di_alarm_time")]
    public decimal? DiAlarmTime { get; set; }

    /// <summary>
    /// Inwersja wejścia
    /// </summary>
    [DisplayName("Inwersja wejścia")]
    [JsonProperty("di_alarm_inv")]
    public string? DiAlarmInv { get; set; }

    /// <summary>
    /// Zatrzymanie sterownika
    /// </summary>
    [DisplayName("Zatrzymanie sterownika")]
    [JsonProperty("di_alarm_stop")]
    public string? DiAlarmStop { get; set; }

    /// <summary>
    /// Czujnik zamknięcia zasobnika
    /// </summary>
    [DisplayName("Czujnik zamknięcia zasobnika")]
    [JsonProperty("di_zas_en")]
    public string? DiZasEn { get; set; }

    /// <summary>
    /// Czas opóźnienia zamknięcia zasobnika
    /// </summary>
    [DisplayName("Czas opóźnienia zamknięcia zasobnika")]
    [JsonProperty("di_zas_delay")]
    public decimal? DiZasDelay { get; set; }

    /// <summary>
    /// Czujnik zerwania zawleczki
    /// </summary>
    [DisplayName("Czujnik zerwania zawleczki")]
    [JsonProperty("di_zaw_en")]
    public string? DiZawEn { get; set; }

    [DisplayName("")]
    [JsonProperty("install_type")]
    public string? InstallType { get; set; }

    /// <summary>
    /// Czas otwarcia zaworu
    /// </summary>
    [DisplayName("Czas otwarcia zaworu")]
    [JsonProperty("zaw4d_open_time")]
    public decimal? Zaw4dOpenTime { get; set; }

    /// <summary>
    /// Histereza pracy zaworu
    /// </summary>
    [DisplayName("Histereza pracy zaworu")]
    [JsonProperty("zaw4d_hist")]
    public decimal? Zaw4dHist { get; set; }

    /// <summary>
    /// Wsp. wzmocnienia
    /// </summary>
    [DisplayName("Wsp. wzmocnienia")]
    [JsonProperty("zaw4d_p")]
    public decimal? Zaw4dP { get; set; }

    /// <summary>
    /// Czas reakcji
    /// </summary>
    [DisplayName("Czas reakcji")]
    [JsonProperty("zaw4d_i")]
    public decimal? Zaw4dI { get; set; }

    /// <summary>
    /// Kierunek otwierania zaworu
    /// </summary>
    [DisplayName("Kierunek otwierania zaworu")]
    [JsonProperty("zaw4d_dir")]
    public string? Zaw4dDir { get; set; }

    /// <summary>
    /// Poziom paliwa w zasobniku
    /// </summary>
    [DisplayName("Poziom paliwa w zasobniku")]
    [JsonProperty("fuel_level")]
    public string? FuelLevel { get; set; }

    /// <summary>
    /// Poziom paliwa w zasobniku
    /// </summary>
    [DisplayName("Poziom paliwa w zasobniku")]
    [JsonProperty("fuel_level_enum")]
    public string? FuelLevelEnum { get; set; }

    [DisplayName("")]
    [JsonProperty("pod_run_time")]
    public string? PodRunTime { get; set; }

    /// <summary>
    /// Czas pracy podajnika
    /// </summary>
    [DisplayName("Czas pracy podajnika")]
    [JsonProperty("pod_run_time_str")]
    public string? PodRunTimeStr { get; set; }

    [DisplayName("")]
    [JsonProperty("pod_run_time_last")]
    public string? PodRunTimeLast { get; set; }

    [DisplayName("")]
    [JsonProperty("pod_run_time_hour")]
    public string? PodRunTimeHour { get; set; }

    /// <summary>
    /// Data kolejnego zasypu
    /// </summary>
    [DisplayName("Data kolejnego zasypu")]
    [JsonProperty("next_fuel_time")]
    public string? NextFuelTime { get; set; }

    [DisplayName("")]
    [JsonProperty("add_fuel")]
    public string? AddFuel { get; set; }

    [DisplayName("")]
    [JsonProperty("add_fuel_time")]
    public string? AddFuelTime { get; set; }

    /// <summary>
    /// Czas do opróźnienia zasobnika
    /// </summary>
    [DisplayName("Czas do opróźnienia zasobnika")]
    [JsonProperty("time_to_empty")]
    public decimal? TimeToEmpty { get; set; }

    [DisplayName("")]
    [JsonProperty("stats_pwr")]
    public string? StatsPwr { get; set; }

    [DisplayName("")]
    [JsonProperty("tr_ec_tkot")]
    public string? TrEcTkot { get; set; }

    [DisplayName("")]
    [JsonProperty("tr_ec_tsp")]
    public string? TrEcTsp { get; set; }

    [DisplayName("")]
    [JsonProperty("tr_gr_tkot")]
    public string? TrGrTkot { get; set; }

    [DisplayName("")]
    [JsonProperty("tr_gr_tsp")]
    public string? TrGrTsp { get; set; }

    [DisplayName("")]
    [JsonProperty("tr_rr_tsp")]
    public string? TrRrTsp { get; set; }

    [DisplayName("")]
    [JsonProperty("daytime")]
    public string? Daytime { get; set; }

    [DisplayName("")]
    [JsonProperty("upd_fr_name")]
    public string? UpdFrName { get; set; }

    [DisplayName("")]
    [JsonProperty("panel_cd1")]
    public string? PanelCd1 { get; set; }

    /// <summary>
    /// Wygaszacz ekranu
    /// </summary>
    [DisplayName("Wygaszacz ekranu")]
    [JsonProperty("en_scr")]
    public string? EnScr { get; set; }

    /// <summary>
    /// Wyjście alarmowe
    /// </summary>
    [DisplayName("Wyjście alarmowe")]
    [JsonProperty("en_ext_out")]
    public string? EnExtOut { get; set; }

    [DisplayName("")]
    [JsonProperty("upd_pgs")]
    public string? UpdPgs { get; set; }

    /// <summary>
    /// Dzień kalibracji
    /// </summary>
    [DisplayName("Dzień kalibracji")]
    [JsonProperty("trv_calib_day")]
    public string? TrvCalibDay { get; set; }

    /// <summary>
    /// Godzina kalibracji
    /// </summary>
    [DisplayName("Godzina kalibracji")]
    [JsonProperty("trv_calib_hour")]
    public string? TrvCalibHour { get; set; }

    /// <summary>
    /// Tryb pracy głowic
    /// </summary>
    [DisplayName("Tryb pracy głowic")]
    [JsonProperty("trv_mode")]
    public string? TrvMode { get; set; }

    /// <summary>
    /// Czas wyłączenia ogrzewania
    /// </summary>
    [DisplayName("Czas wyłączenia ogrzewania")]
    [JsonProperty("wnd_time")]
    public int? WndTime { get; set; }

    /// <summary>
    /// Histereza temperatury
    /// </summary>
    [DisplayName("Histereza temperatury")]
    [JsonProperty("wnd_hist")]
    public decimal? WndHist { get; set; }

    [DisplayName("")]
    [JsonProperty("wnd_cfg")]
    public string? WndCfg { get; set; }

    /// <summary>
    /// Maksymalny kąt otwarcia zaworu 4D
    /// </summary>
    [DisplayName("Maksymalny kąt otwarcia zaworu 4D")]
    [JsonProperty("ob1_zaw4d_max")]
    public decimal? Ob1Zaw4dMax { get; set; }

    /// <summary>
    /// Typ obwodu
    /// </summary>
    [DisplayName("Typ obwodu")]
    [JsonProperty("ob3_typ")]
    public string? Ob3Typ { get; set; }

    /// <summary>
    /// Programator temp. wewnętrznej
    /// </summary>
    [DisplayName("Programator temp. wewnętrznej")]
    [JsonProperty("ob3_prog")]
    public string? Ob3Prog { get; set; }

    /// <summary>
    /// Temperatura zadana
    /// </summary>
    [DisplayName("Temperatura zadana")]
    [JsonProperty("ob3_tzad")]
    public decimal? Ob3Tzad { get; set; }

    /// <summary>
    /// Temperatura obniżona
    /// </summary>
    [DisplayName("Temperatura obniżona")]
    [JsonProperty("ob3_tobn")]
    public decimal? Ob3Tobn { get; set; }

    /// <summary>
    /// Temperatura maksymalna
    /// </summary>
    [DisplayName("Temperatura maksymalna")]
    [JsonProperty("ob3_tmax")]
    public decimal? Ob3Tmax { get; set; }

    /// <summary>
    /// Czas pracy pompy
    /// </summary>
    [DisplayName("Czas pracy pompy")]
    [JsonProperty("ob3_pomp_on")]
    public decimal? Ob3PompOn { get; set; }

    /// <summary>
    /// Czas postoju pompy
    /// </summary>
    [DisplayName("Czas postoju pompy")]
    [JsonProperty("ob3_pomp_off")]
    public decimal? Ob3PompOff { get; set; }

    /// <summary>
    /// Temperatura wewn. niska
    /// </summary>
    [DisplayName("Temperatura wewn. niska")]
    [JsonProperty("ob3_pok_lo")]
    public decimal? Ob3PokLo { get; set; }

    /// <summary>
    /// Temperatura wewn. normalna
    /// </summary>
    [DisplayName("Temperatura wewn. normalna")]
    [JsonProperty("ob3_pok_norm")]
    public decimal? Ob3PokNorm { get; set; }

    /// <summary>
    /// Temperatura wewn. komfortowa
    /// </summary>
    [DisplayName("Temperatura wewn. komfortowa")]
    [JsonProperty("ob3_pok_hi")]
    public decimal? Ob3PokHi { get; set; }

    /// <summary>
    /// Histereza temp. wewn.
    /// </summary>
    [DisplayName("Histereza temp. wewn.")]
    [JsonProperty("ob3_pok_hist")]
    public decimal? Ob3PokHist { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_pok_pre")]
    public string? Ob3PokPre { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_pok_tact")]
    public string? Ob3PokTact { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_pok_tzad")]
    public string? Ob3PokTzad { get; set; }

    /// <summary>
    /// Typ regulatora pokojowego
    /// </summary>
    [DisplayName("Typ regulatora pokojowego")]
    [JsonProperty("ob3_pok_typ")]
    public string? Ob3PokTyp { get; set; }

    /// <summary>
    /// Programator temp. za zaworem
    /// </summary>
    [DisplayName("Programator temp. za zaworem")]
    [JsonProperty("ob3_zaw4d_prog")]
    public string? Ob3Zaw4dProg { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_zaw4d_tzad")]
    public string? Ob3Zaw4dTzad { get; set; }

    /// <summary>
    /// Regulator pogodowy
    /// </summary>
    [DisplayName("Regulator pogodowy")]
    [JsonProperty("ob3_pog_en")]
    public string? Ob3PogEn { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. dla -10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. dla -10°C")]
    [JsonProperty("ob3_pog_krzyw1")]
    public decimal? Ob3PogKrzyw1 { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. dla +10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. dla +10°C")]
    [JsonProperty("ob3_pog_krzyw2")]
    public decimal? Ob3PogKrzyw2 { get; set; }

    /// <summary>
    /// Zatrzymanie pompy po dogrzaniu pomieszczenia
    /// </summary>
    [DisplayName("Zatrzymanie pompy po dogrzaniu pomieszczenia")]
    [JsonProperty("ob3_pump_pok")]
    public string? Ob3PumpPok { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_zaw4d_pos")]
    public string? Ob3Zaw4dPos { get; set; }

    /// <summary>
    /// Maksymalny kąt otwarcia zaworu 4D
    /// </summary>
    [DisplayName("Maksymalny kąt otwarcia zaworu 4D")]
    [JsonProperty("ob3_zaw4d_max")]
    public decimal? Ob3Zaw4dMax { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_out_pump")]
    public string? Ob3OutPump { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_out_zaw4d")]
    public string? Ob3OutZaw4d { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_t1_alarm")]
    public string? Ob3T1Alarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_t2_alarm")]
    public string? Ob3T2Alarm { get; set; }

    /// <summary>
    /// Temperatura za zaworem CO3
    /// </summary>
    [DisplayName("Temperatura za zaworem CO3")]
    [JsonProperty("ob3_t1")]
    public decimal? Ob3T1 { get; set; }

    /// <summary>
    /// Temperatura wewnętrzna CO3
    /// </summary>
    [DisplayName("Temperatura wewnętrzna CO3")]
    [JsonProperty("ob3_t2")]
    public decimal? Ob3T2 { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik T1 w CO3
    /// </summary>
    [DisplayName("Poprawka kalibracyjna - czujnik T1 w CO3")]
    [JsonProperty("ob3_t1_cal")]
    public decimal? Ob3T1Cal { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik T2 w CO3
    /// </summary>
    [DisplayName("Poprawka kalibracyjna - czujnik T2 w CO3")]
    [JsonProperty("ob3_t2_cal")]
    public decimal? Ob3T2Cal { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_hitemp_alarm")]
    public string? Ob3HitempAlarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_mr3_alarm")]
    public string? Ob3Mr3Alarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_di_term")]
    public string? Ob3DiTerm { get; set; }

    /// <summary>
    /// Czas otwarcia zaworu
    /// </summary>
    [DisplayName("Czas otwarcia zaworu")]
    [JsonProperty("ob3_zaw4d_open_time")]
    public decimal? Ob3Zaw4dOpenTime { get; set; }

    /// <summary>
    /// Histereza pracy zaworu
    /// </summary>
    [DisplayName("Histereza pracy zaworu")]
    [JsonProperty("ob3_zaw4d_hist")]
    public decimal? Ob3Zaw4dHist { get; set; }

    /// <summary>
    /// Wsp. wzmocnienia
    /// </summary>
    [DisplayName("Wsp. wzmocnienia")]
    [JsonProperty("ob3_zaw4d_p")]
    public decimal? Ob3Zaw4dP { get; set; }

    /// <summary>
    /// Czas reakcji
    /// </summary>
    [DisplayName("Czas reakcji")]
    [JsonProperty("ob3_zaw4d_i")]
    public decimal? Ob3Zaw4dI { get; set; }

    /// <summary>
    /// Kierunek otwierania zaworu
    /// </summary>
    [DisplayName("Kierunek otwierania zaworu")]
    [JsonProperty("ob3_zaw4d_dir")]
    public string? Ob3Zaw4dDir { get; set; }

    /// <summary>
    /// Typ obwodu
    /// </summary>
    [DisplayName("Typ obwodu")]
    [JsonProperty("ob4_typ")]
    public string? Ob4Typ { get; set; }

    /// <summary>
    /// Programator temp. wewnętrznej
    /// </summary>
    [DisplayName("Programator temp. wewnętrznej")]
    [JsonProperty("ob4_prog")]
    public string? Ob4Prog { get; set; }

    /// <summary>
    /// Temperatura zadana
    /// </summary>
    [DisplayName("Temperatura zadana")]
    [JsonProperty("ob4_tzad")]
    public decimal? Ob4Tzad { get; set; }

    /// <summary>
    /// Temperatura obniżona
    /// </summary>
    [DisplayName("Temperatura obniżona")]
    [JsonProperty("ob4_tobn")]
    public decimal? Ob4Tobn { get; set; }

    /// <summary>
    /// Temperatura maksymalna
    /// </summary>
    [DisplayName("Temperatura maksymalna")]
    [JsonProperty("ob4_tmax")]
    public decimal? Ob4Tmax { get; set; }

    /// <summary>
    /// Czas pracy pompy
    /// </summary>
    [DisplayName("Czas pracy pompy")]
    [JsonProperty("ob4_pomp_on")]
    public decimal? Ob4PompOn { get; set; }

    /// <summary>
    /// Czas postoju pompy
    /// </summary>
    [DisplayName("Czas postoju pompy")]
    [JsonProperty("ob4_pomp_off")]
    public decimal? Ob4PompOff { get; set; }

    /// <summary>
    /// Temperatura wewn. niska
    /// </summary>
    [DisplayName("Temperatura wewn. niska")]
    [JsonProperty("ob4_pok_lo")]
    public decimal? Ob4PokLo { get; set; }

    /// <summary>
    /// Temperatura wewn. normalna
    /// </summary>
    [DisplayName("Temperatura wewn. normalna")]
    [JsonProperty("ob4_pok_norm")]
    public decimal? Ob4PokNorm { get; set; }

    /// <summary>
    /// Temperatura wewn. komfortowa
    /// </summary>
    [DisplayName("Temperatura wewn. komfortowa")]
    [JsonProperty("ob4_pok_hi")]
    public decimal? Ob4PokHi { get; set; }

    /// <summary>
    /// Histereza temp. wewn.
    /// </summary>
    [DisplayName("Histereza temp. wewn.")]
    [JsonProperty("ob4_pok_hist")]
    public decimal? Ob4PokHist { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_pok_pre")]
    public string? Ob4PokPre { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_pok_tact")]
    public string? Ob4PokTact { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_pok_tzad")]
    public string? Ob4PokTzad { get; set; }

    /// <summary>
    /// Typ regulatora pokojowego
    /// </summary>
    [DisplayName("Typ regulatora pokojowego")]
    [JsonProperty("ob4_pok_typ")]
    public string? Ob4PokTyp { get; set; }

    /// <summary>
    /// Programator temp. za zaworem
    /// </summary>
    [DisplayName("Programator temp. za zaworem")]
    [JsonProperty("ob4_zaw4d_prog")]
    public string? Ob4Zaw4dProg { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_zaw4d_tzad")]
    public string? Ob4Zaw4dTzad { get; set; }

    /// <summary>
    /// Regulator pogodowy
    /// </summary>
    [DisplayName("Regulator pogodowy")]
    [JsonProperty("ob4_pog_en")]
    public string? Ob4PogEn { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. dla -10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. dla -10°C")]
    [JsonProperty("ob4_pog_krzyw1")]
    public decimal? Ob4PogKrzyw1 { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. dla +10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. dla +10°C")]
    [JsonProperty("ob4_pog_krzyw2")]
    public decimal? Ob4PogKrzyw2 { get; set; }

    /// <summary>
    /// Zatrzymanie pompy po dogrzaniu pomieszczenia
    /// </summary>
    [DisplayName("Zatrzymanie pompy po dogrzaniu pomieszczenia")]
    [JsonProperty("ob4_pump_pok")]
    public string? Ob4PumpPok { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_zaw4d_pos")]
    public string? Ob4Zaw4dPos { get; set; }

    /// <summary>
    /// Maksymalny kąt otwarcia zaworu 4D
    /// </summary>
    [DisplayName("Maksymalny kąt otwarcia zaworu 4D")]
    [JsonProperty("ob4_zaw4d_max")]
    public decimal? Ob4Zaw4dMax { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_out_pump")]
    public string? Ob4OutPump { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_out_zaw4d")]
    public string? Ob4OutZaw4d { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_t1_alarm")]
    public string? Ob4T1Alarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_t2_alarm")]
    public string? Ob4T2Alarm { get; set; }

    /// <summary>
    /// Temperatura za zaworem CO4
    /// </summary>
    [DisplayName("Temperatura za zaworem CO4")]
    [JsonProperty("ob4_t1")]
    public decimal? Ob4T1 { get; set; }

    /// <summary>
    /// Temperatura wewnętrzna CO4
    /// </summary>
    [DisplayName("Temperatura wewnętrzna CO4")]
    [JsonProperty("ob4_t2")]
    public decimal? Ob4T2 { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik T1 w CO4
    /// </summary>
    [DisplayName("Poprawka kalibracyjna - czujnik T1 w CO4")]
    [JsonProperty("ob4_t1_cal")]
    public decimal? Ob4T1Cal { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik T2 w CO4
    /// </summary>
    [DisplayName("Poprawka kalibracyjna - czujnik T2 w CO4")]
    [JsonProperty("ob4_t2_cal")]
    public decimal? Ob4T2Cal { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_hitemp_alarm")]
    public string? Ob4HitempAlarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_mr3_alarm")]
    public string? Ob4Mr3Alarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_di_term")]
    public string? Ob4DiTerm { get; set; }

    /// <summary>
    /// Czas otwarcia zaworu
    /// </summary>
    [DisplayName("Czas otwarcia zaworu")]
    [JsonProperty("ob4_zaw4d_open_time")]
    public decimal? Ob4Zaw4dOpenTime { get; set; }

    /// <summary>
    /// Histereza pracy zaworu
    /// </summary>
    [DisplayName("Histereza pracy zaworu")]
    [JsonProperty("ob4_zaw4d_hist")]
    public decimal? Ob4Zaw4dHist { get; set; }

    /// <summary>
    /// Wsp. wzmocnienia
    /// </summary>
    [DisplayName("Wsp. wzmocnienia")]
    [JsonProperty("ob4_zaw4d_p")]
    public decimal? Ob4Zaw4dP { get; set; }

    /// <summary>
    /// Czas reakcji
    /// </summary>
    [DisplayName("Czas reakcji")]
    [JsonProperty("ob4_zaw4d_i")]
    public decimal? Ob4Zaw4dI { get; set; }

    /// <summary>
    /// Kierunek otwierania zaworu
    /// </summary>
    [DisplayName("Kierunek otwierania zaworu")]
    [JsonProperty("ob4_zaw4d_dir")]
    public string? Ob4Zaw4dDir { get; set; }

    /// <summary>
    /// Typ obwodu
    /// </summary>
    [DisplayName("Typ obwodu")]
    [JsonProperty("ob5_typ")]
    public string? Ob5Typ { get; set; }

    /// <summary>
    /// Programator temp. wewnętrznej
    /// </summary>
    [DisplayName("Programator temp. wewnętrznej")]
    [JsonProperty("ob5_prog")]
    public string? Ob5Prog { get; set; }

    /// <summary>
    /// Temperatura zadana
    /// </summary>
    [DisplayName("Temperatura zadana")]
    [JsonProperty("ob5_tzad")]
    public decimal? Ob5Tzad { get; set; }

    /// <summary>
    /// Temperatura obniżona
    /// </summary>
    [DisplayName("Temperatura obniżona")]
    [JsonProperty("ob5_tobn")]
    public decimal? Ob5Tobn { get; set; }

    /// <summary>
    /// Temperatura maksymalna
    /// </summary>
    [DisplayName("Temperatura maksymalna")]
    [JsonProperty("ob5_tmax")]
    public decimal? Ob5Tmax { get; set; }

    /// <summary>
    /// Czas pracy pompy
    /// </summary>
    [DisplayName("Czas pracy pompy")]
    [JsonProperty("ob5_pomp_on")]
    public decimal? Ob5PompOn { get; set; }

    /// <summary>
    /// Czas postoju pompy
    /// </summary>
    [DisplayName("Czas postoju pompy")]
    [JsonProperty("ob5_pomp_off")]
    public decimal? Ob5PompOff { get; set; }

    /// <summary>
    /// Temperatura wewn. niska
    /// </summary>
    [DisplayName("Temperatura wewn. niska")]
    [JsonProperty("ob5_pok_lo")]
    public decimal? Ob5PokLo { get; set; }

    /// <summary>
    /// Temperatura wewn. normalna
    /// </summary>
    [DisplayName("Temperatura wewn. normalna")]
    [JsonProperty("ob5_pok_norm")]
    public decimal? Ob5PokNorm { get; set; }

    /// <summary>
    /// Temperatura wewn. komfortowa
    /// </summary>
    [DisplayName("Temperatura wewn. komfortowa")]
    [JsonProperty("ob5_pok_hi")]
    public decimal? Ob5PokHi { get; set; }

    /// <summary>
    /// Histereza temp. wewn.
    /// </summary>
    [DisplayName("Histereza temp. wewn.")]
    [JsonProperty("ob5_pok_hist")]
    public decimal? Ob5PokHist { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_pok_pre")]
    public string? Ob5PokPre { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_pok_tact")]
    public string? Ob5PokTact { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_pok_tzad")]
    public string? Ob5PokTzad { get; set; }

    /// <summary>
    /// Typ regulatora pokojowego
    /// </summary>
    [DisplayName("Typ regulatora pokojowego")]
    [JsonProperty("ob5_pok_typ")]
    public string? Ob5PokTyp { get; set; }

    /// <summary>
    /// Programator temp. za zaworem
    /// </summary>
    [DisplayName("Programator temp. za zaworem")]
    [JsonProperty("ob5_zaw4d_prog")]
    public string? Ob5Zaw4dProg { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_zaw4d_tzad")]
    public string? Ob5Zaw4dTzad { get; set; }

    /// <summary>
    /// Regulator pogodowy
    /// </summary>
    [DisplayName("Regulator pogodowy")]
    [JsonProperty("ob5_pog_en")]
    public string? Ob5PogEn { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. dla -10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. dla -10°C")]
    [JsonProperty("ob5_pog_krzyw1")]
    public decimal? Ob5PogKrzyw1 { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. dla +10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. dla +10°C")]
    [JsonProperty("ob5_pog_krzyw2")]
    public decimal? Ob5PogKrzyw2 { get; set; }

    /// <summary>
    /// Zatrzymanie pompy po dogrzaniu pomieszczenia
    /// </summary>
    [DisplayName("Zatrzymanie pompy po dogrzaniu pomieszczenia")]
    [JsonProperty("ob5_pump_pok")]
    public string? Ob5PumpPok { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_zaw4d_pos")]
    public string? Ob5Zaw4dPos { get; set; }

    /// <summary>
    /// Maksymalny kąt otwarcia zaworu 4D
    /// </summary>
    [DisplayName("Maksymalny kąt otwarcia zaworu 4D")]
    [JsonProperty("ob5_zaw4d_max")]
    public decimal? Ob5Zaw4dMax { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_out_pump")]
    public string? Ob5OutPump { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_out_zaw4d")]
    public string? Ob5OutZaw4d { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_t1_alarm")]
    public string? Ob5T1Alarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_t2_alarm")]
    public string? Ob5T2Alarm { get; set; }

    /// <summary>
    /// Temperatura za zaworem w CO5
    /// </summary>
    [DisplayName("Temperatura za zaworem w CO5")]
    [JsonProperty("ob5_t1")]
    public decimal? Ob5T1 { get; set; }

    /// <summary>
    /// Temperatura wewnętrzna CO5
    /// </summary>
    [DisplayName("Temperatura wewnętrzna CO5")]
    [JsonProperty("ob5_t2")]
    public decimal? Ob5T2 { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik T1 w CO5
    /// </summary>
    [DisplayName("Poprawka kalibracyjna - czujnik T1 w CO5")]
    [JsonProperty("ob5_t1_cal")]
    public decimal? Ob5T1Cal { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik T2 w CO5
    /// </summary>
    [DisplayName("Poprawka kalibracyjna - czujnik T2 w CO5")]
    [JsonProperty("ob5_t2_cal")]
    public decimal? Ob5T2Cal { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_hitemp_alarm")]
    public string? Ob5HitempAlarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_mr3_alarm")]
    public string? Ob5Mr3Alarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_di_term")]
    public string? Ob5DiTerm { get; set; }

    /// <summary>
    /// Czas otwarcia zaworu
    /// </summary>
    [DisplayName("Czas otwarcia zaworu")]
    [JsonProperty("ob5_zaw4d_open_time")]
    public decimal? Ob5Zaw4dOpenTime { get; set; }

    /// <summary>
    /// Histereza pracy zaworu
    /// </summary>
    [DisplayName("Histereza pracy zaworu")]
    [JsonProperty("ob5_zaw4d_hist")]
    public decimal? Ob5Zaw4dHist { get; set; }

    /// <summary>
    /// Wsp. wzmocnienia
    /// </summary>
    [DisplayName("Wsp. wzmocnienia")]
    [JsonProperty("ob5_zaw4d_p")]
    public decimal? Ob5Zaw4dP { get; set; }

    /// <summary>
    /// Czas reakcji
    /// </summary>
    [DisplayName("Czas reakcji")]
    [JsonProperty("ob5_zaw4d_i")]
    public decimal? Ob5Zaw4dI { get; set; }

    /// <summary>
    /// Kierunek otwierania zaworu
    /// </summary>
    [DisplayName("Kierunek otwierania zaworu")]
    [JsonProperty("ob5_zaw4d_dir")]
    public string? Ob5Zaw4dDir { get; set; }

    /// <summary>
    /// Typ obwodu
    /// </summary>
    [DisplayName("Typ obwodu")]
    [JsonProperty("ob6_typ")]
    public string? Ob6Typ { get; set; }

    /// <summary>
    /// Programator temp. wewnętrznej
    /// </summary>
    [DisplayName("Programator temp. wewnętrznej")]
    [JsonProperty("ob6_prog")]
    public string? Ob6Prog { get; set; }

    /// <summary>
    /// Temperatura zadana
    /// </summary>
    [DisplayName("Temperatura zadana")]
    [JsonProperty("ob6_tzad")]
    public decimal? Ob6Tzad { get; set; }

    /// <summary>
    /// Temperatura obniżona
    /// </summary>
    [DisplayName("Temperatura obniżona")]
    [JsonProperty("ob6_tobn")]
    public decimal? Ob6Tobn { get; set; }

    /// <summary>
    /// Temperatura maksymalna
    /// </summary>
    [DisplayName("Temperatura maksymalna")]
    [JsonProperty("ob6_tmax")]
    public decimal? Ob6Tmax { get; set; }

    /// <summary>
    /// Czas pracy pompy
    /// </summary>
    [DisplayName("Czas pracy pompy")]
    [JsonProperty("ob6_pomp_on")]
    public decimal? Ob6PompOn { get; set; }

    /// <summary>
    /// Czas postoju pompy
    /// </summary>
    [DisplayName("Czas postoju pompy")]
    [JsonProperty("ob6_pomp_off")]
    public decimal? Ob6PompOff { get; set; }

    /// <summary>
    /// Temperatura wewn. niska
    /// </summary>
    [DisplayName("Temperatura wewn. niska")]
    [JsonProperty("ob6_pok_lo")]
    public decimal? Ob6PokLo { get; set; }

    /// <summary>
    /// Temperatura wewn. normalna
    /// </summary>
    [DisplayName("Temperatura wewn. normalna")]
    [JsonProperty("ob6_pok_norm")]
    public decimal? Ob6PokNorm { get; set; }

    /// <summary>
    /// Temperatura wewn. komfortowa
    /// </summary>
    [DisplayName("Temperatura wewn. komfortowa")]
    [JsonProperty("ob6_pok_hi")]
    public decimal? Ob6PokHi { get; set; }

    /// <summary>
    /// Histereza temp. wewn.
    /// </summary>
    [DisplayName("Histereza temp. wewn.")]
    [JsonProperty("ob6_pok_hist")]
    public decimal? Ob6PokHist { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_pok_pre")]
    public string? Ob6PokPre { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_pok_tact")]
    public string? Ob6PokTact { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_pok_tzad")]
    public string? Ob6PokTzad { get; set; }

    /// <summary>
    /// Typ regulatora pokojowego
    /// </summary>
    [DisplayName("Typ regulatora pokojowego")]
    [JsonProperty("ob6_pok_typ")]
    public string? Ob6PokTyp { get; set; }

    /// <summary>
    /// Programator temp. za zaworem
    /// </summary>
    [DisplayName("Programator temp. za zaworem")]
    [JsonProperty("ob6_zaw4d_prog")]
    public string? Ob6Zaw4dProg { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_zaw4d_tzad")]
    public string? Ob6Zaw4dTzad { get; set; }

    /// <summary>
    /// Regulator pogodowy
    /// </summary>
    [DisplayName("Regulator pogodowy")]
    [JsonProperty("ob6_pog_en")]
    public string? Ob6PogEn { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. dla -10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. dla -10°C")]
    [JsonProperty("ob6_pog_krzyw1")]
    public decimal? Ob6PogKrzyw1 { get; set; }

    /// <summary>
    /// Krzywa grzania - temp. dla +10°C
    /// </summary>
    [DisplayName("Krzywa grzania - temp. dla +10°C")]
    [JsonProperty("ob6_pog_krzyw2")]
    public decimal? Ob6PogKrzyw2 { get; set; }

    /// <summary>
    /// Zatrzymanie pompy po dogrzaniu pomieszczenia
    /// </summary>
    [DisplayName("Zatrzymanie pompy po dogrzaniu pomieszczenia")]
    [JsonProperty("ob6_pump_pok")]
    public string? Ob6PumpPok { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_zaw4d_pos")]
    public string? Ob6Zaw4dPos { get; set; }

    /// <summary>
    /// Maksymalny kąt otwarcia zaworu 4D
    /// </summary>
    [DisplayName("Maksymalny kąt otwarcia zaworu 4D")]
    [JsonProperty("ob6_zaw4d_max")]
    public decimal? Ob6Zaw4dMax { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_out_pump")]
    public string? Ob6OutPump { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_out_zaw4d")]
    public string? Ob6OutZaw4d { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_t1_alarm")]
    public string? Ob6T1Alarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_t2_alarm")]
    public string? Ob6T2Alarm { get; set; }

    /// <summary>
    /// Temperatura za zaworem w CO6
    /// </summary>
    [DisplayName("Temperatura za zaworem w CO6")]
    [JsonProperty("ob6_t1")]
    public decimal? Ob6T1 { get; set; }

    /// <summary>
    /// Temperatura wewnętrzna CO6
    /// </summary>
    [DisplayName("Temperatura wewnętrzna CO6")]
    [JsonProperty("ob6_t2")]
    public decimal? Ob6T2 { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik T1 w CO6
    /// </summary>
    [DisplayName("Poprawka kalibracyjna - czujnik T1 w CO6")]
    [JsonProperty("ob6_t1_cal")]
    public decimal? Ob6T1Cal { get; set; }

    /// <summary>
    /// Poprawka kalibracyjna - czujnik T2 w CO6
    /// </summary>
    [DisplayName("Poprawka kalibracyjna - czujnik T2 w CO6")]
    [JsonProperty("ob6_t2_cal")]
    public decimal? Ob6T2Cal { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_hitemp_alarm")]
    public string? Ob6HitempAlarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_mr3_alarm")]
    public string? Ob6Mr3Alarm { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_di_term")]
    public string? Ob6DiTerm { get; set; }

    /// <summary>
    /// Czas otwarcia zaworu
    /// </summary>
    [DisplayName("Czas otwarcia zaworu")]
    [JsonProperty("ob6_zaw4d_open_time")]
    public decimal? Ob6Zaw4dOpenTime { get; set; }

    /// <summary>
    /// Histereza pracy zaworu
    /// </summary>
    [DisplayName("Histereza pracy zaworu")]
    [JsonProperty("ob6_zaw4d_hist")]
    public decimal? Ob6Zaw4dHist { get; set; }

    /// <summary>
    /// Wsp. wzmocnienia
    /// </summary>
    [DisplayName("Wsp. wzmocnienia")]
    [JsonProperty("ob6_zaw4d_p")]
    public decimal? Ob6Zaw4dP { get; set; }

    /// <summary>
    /// Czas reakcji
    /// </summary>
    [DisplayName("Czas reakcji")]
    [JsonProperty("ob6_zaw4d_i")]
    public decimal? Ob6Zaw4dI { get; set; }

    /// <summary>
    /// Kierunek otwierania zaworu
    /// </summary>
    [DisplayName("Kierunek otwierania zaworu")]
    [JsonProperty("ob6_zaw4d_dir")]
    public string? Ob6Zaw4dDir { get; set; }

    [DisplayName("")]
    [JsonProperty("prod_date")]
    public string? ProdDate { get; set; }

    [DisplayName("")]
    [JsonProperty("rf_status")]
    public string? RfStatus { get; set; }

    [DisplayName("")]
    [JsonProperty("ec_state")]
    public string? EcState { get; set; }

    [DisplayName("")]
    [JsonProperty("ec_podpr")]
    public string? EcPodpr { get; set; }

    [DisplayName("")]
    [JsonProperty("node_add")]
    public string? NodeAdd { get; set; }

    [DisplayName("")]
    [JsonProperty("node_del")]
    public string? NodeDel { get; set; }

    [DisplayName("")]
    [JsonProperty("node_st")]
    public string? NodeSt { get; set; }

    [DisplayName("")]
    [JsonProperty("node_time")]
    public string? NodeTime { get; set; }

    [DisplayName("")]
    [JsonProperty("ob1_pok_heat")]
    public string? Ob1PokHeat { get; set; }

    [DisplayName("")]
    [JsonProperty("ob2_pok_heat")]
    public string? Ob2PokHeat { get; set; }

    [DisplayName("")]
    [JsonProperty("ob3_pok_heat")]
    public string? Ob3PokHeat { get; set; }

    [DisplayName("")]
    [JsonProperty("ob4_pok_heat")]
    public string? Ob4PokHeat { get; set; }

    [DisplayName("")]
    [JsonProperty("ob5_pok_heat")]
    public string? Ob5PokHeat { get; set; }

    [DisplayName("")]
    [JsonProperty("ob6_pok_heat")]
    public string? Ob6PokHeat { get; set; }

    /*
     		<register priv="002" tid="cmd_pair" datatype="cmd" cmdurl="https://esterownik.pl/paruj-sterownik?sn=$device_sn&amp;pairhash=$randomhash" name.pl="Powiąż z kontem"  />
			<register priv="002" tid="cmd_update" datatype="cmd" name.pl="Aktualizacja sterownika"  />
			<register priv="002" tid="cmd_updatecd1" datatype="cmd" name.pl="Aktualizacja panela LCD"  />
			<register priv="002" tid="cmd_reset" datatype="cmd" name.pl="Reset urządzenia"  />
			<register priv="002" tid="cmd_restoredefaut" datatype="cmd" name.pl="Domyślne ustawienia sieci ETHERNET"  />
			<register priv="002" tid="cmd_ackalarm" datatype="cmd" name.pl="Potwierdź alarmy"  />
			<register priv="002" tid="cmd_formatsdcard" datatype="cmd" name.pl="Format karty SD"  />
			<register priv="002" tid="cmd_hardwarereset" datatype="cmd" name.pl="Ustawienia fabryczne"  />
     */

}