using eSterownikMapper;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

#if DEBUG

StaticValues.URL = app.Configuration["Url"];
StaticValues.IpAddress = app.Configuration["IpAddress"];
StaticValues.Login = app.Configuration["Login"];
StaticValues.Password = app.Configuration["Password"];

#else

StaticValues.URL = Environment.GetEnvironmentVariable("Url");
StaticValues.IpAddress = Environment.GetEnvironmentVariable("IpAddress");
StaticValues.Login = Environment.GetEnvironmentVariable("Login");
StaticValues.Password = Environment.GetEnvironmentVariable("Password");

#endif

app.Run();
