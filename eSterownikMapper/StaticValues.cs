﻿using eSterownikMapper.Model;

namespace eSterownikMapper;

public class StaticValues
{
    public static string IpAddress { get; set; }
    public static string URL { get; set; }
    public static string Login { get; set; }
    public static string Password { get; set; }

    public static DateTime? LastDownload { get; set; }
    public static ECoalSync? LastECoalSync { get; set; }
}