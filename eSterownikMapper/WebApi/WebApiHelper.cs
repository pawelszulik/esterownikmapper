﻿using System.Net;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace eSterownikMapper.WebApi;

public static class WebApiHelper
{
    private static int Timeout => 1000 * 15;
    private static IRestRequest request = new RestRequest(Method.GET);

    public static T? GetDataFromWeb<T>(string url, string? userName, string? password)
    {
        T? result = default;

        try
        {
            RestClient client = new RestClient(url)
            {
                Timeout = Timeout
            };
            if (userName != null && password != null)
            {
                client.Authenticator = new HttpBasicAuthenticator(userName, password);
            }
            

            IRestResponse<T> response = client.Get<T>(request);

            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Accepted)
                throw new Exception(response.ErrorMessage);

            if (response is RestResponse<T> restResponse)
            {
                
                if (restResponse.Data == null && (typeof(T) == typeof(String)))
                {
                    result = (T)(object)restResponse.Content;
                }
                else
                {
                    result = restResponse.Data;

                }
            }
            else
                result = JsonConvert.DeserializeObject<T>(response.Content);
        }
        catch (WebException exception)
        {
            Console.WriteLine($"{exception.GetType().Name} - {typeof(WebApiHelper)} - {exception.Message}");
            throw exception;
        }
        catch (Exception exception)
        {
            Console.WriteLine($"{exception.GetType().Name} - {typeof(WebApiHelper)} - {exception.Message}");
            throw exception;
        }

        return result;
    }
    public static T? SetData<T>(string ipAddress, string? userName, string? password, string property, string value)
    {
        T? result = default;

        try
        {
            RestClient client = new RestClient($"http://{ipAddress}/setregister.cgi?device=0&{property}={value}")
            {
                Timeout = Timeout
            };
            if (userName != null && password != null)
            {
                client.Authenticator = new HttpBasicAuthenticator(userName, password);
            }

            IRestResponse<T> response = client.Get<T>(request);

            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Accepted)
                throw new Exception(response.ErrorMessage);

            if (response is RestResponse<T> restResponse)
            {
                
                if (restResponse.Data == null && (typeof(T) == typeof(String)))
                {
                    result = (T)(object)restResponse.Content;
                }
                else
                {
                    result = restResponse.Data;

                }
            }
            else
                result = JsonConvert.DeserializeObject<T>(response.Content);
        }
        catch (WebException exception)
        {
            Console.WriteLine($"{exception.GetType().Name} - {typeof(WebApiHelper)} - {exception.Message}");
            throw exception;
        }
        catch (Exception exception)
        {
            Console.WriteLine($"{exception.GetType().Name} - {typeof(WebApiHelper)} - {exception.Message}");
            throw exception;
        }

        return result;
    }
}